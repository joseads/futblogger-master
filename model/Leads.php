<?php
 
class Leads {
 
    private $id_lead;
    private $nome;
    private $email;
    private $senha;
    private $valido;
 
    public function getId_lead() {
        return $this->id_lead;
    }
 
    public function setId_lead($id_lead) {
        $this->id_lead = $id_lead;
    }
 
    public function getNome() {
        return $this->nome;
    }
 
    public function setNome($nome) {
        $this->nome = $nome;
    }
 
    public function getEmail() {
        return $this->email;
    }
 
    public function setEmail($email) {
        $this->email = strtolower($email);
    }
        
    public function getIp() {
        return $this->ip;
    }
    
    public function setIp($ip) {
        $this->ip = $ip;
    }
         
    public function getData() {
        return $this->data;
    }
    
    public function setData($data) {
        $this->data = $data;
    }
    public function getSenha() {
        return $this->senha;
    }
 
    public function setSenha($senha) {
        $this->senha = strtolower($senha);
    }    
     
    public function getValido() {
        return $this->valido;
    }
 
    public function setValido($valido) {
        $this->valido = strtolower($valido);
    }
 
}
 
?>