
<!DOCTYPE html>
<html lang="pt-br">
    <head>
     <meta charset="utf-8">
        
      <link rel="stylesheet" href="../css/bootstrap.min.css"  type="text/css" >
      <link rel="stylesheet" href="../css/centraliza.css"  type="text/css" >
        <!--  <link href="../css/teste.css" rel="stylesheet" -->

<!-- Custom styles for this template -->
    <link href="pricing.css" rel="stylesheet">
  </head>
    <body>
        <?php include "header.php"; ?>
        <main role="main">

  <!-- Main jumbotron for a primary marketing message or call to action -->
  <div class="jumbotron"> 
      
    <div class="container">
      <h3 class="display-12">Post</h3>
      <h2 class="display-12">Confira os jogos de futebol hoje,</h2>
      <h2 class="display-12">domingo, 6 de outubro (06/10)</h2>
      <p>Por Luiz Ferreira</p>
      <p>06/10/19</p>
    </div>
  <img src="../img/Futebol-2.jpg" class="img-fluid" alt="Responsive image" style="width: 100%">
  <div class="container">
    <!-- Example row of columns -->
    <div class="row">
      <div class="col-md-4">
        <h2>Heading</h2>
        <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
        <p><a class="btn btn-secondary" href="../controller/PostBlog.php?file=1" role="button">View details &raquo;</a></p>
      </div>
      <div class="col-md-4">
        <h2>Heading</h2>
        <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
        <p><a class="btn btn-secondary" href="#" role="button">View details &raquo;</a></p>
      </div>
      <div class="col-md-4">
        <h2>Heading</h2>
        <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
        <p><a class="btn btn-secondary" href="#" role="button">View details &raquo;</a></p>
      </div>
    </div>

    <hr>

  </div> <!-- /container -->
      


        
        
        <div class="centraliza">
            <form method="post" action="../controller/TesteController.php">
            <input type="text" name="nome" placeholder="Usúario">
            <input type="email" name="email" placeholder="Email">
            <input type="submit" value="Cadastrar">
        </form>
        </div>
</div>
</main>
        <?php include "footer.php"; ?> 
     </body>
</html>
    
