<?php
 
class LeadsDao 
{
    private $db;
    public function __construct(Database $db)
    {
        $this->db = $db;
    }

    public function add(Leads $lead) 
    {
        $nome = $lead->getNome();
        $email = $lead->getEmail();
        $data = $lead->getData();
        $ip = $lead->getIp();
        
        //$valido = $lead->getValido(1);

        $query = "INSERT INTO lead (nome, email, data, ip ) VALUES(?,?, ?, ?)"; 
        $stmt = mysqli_prepare($this->db->getConection(), $query);
        mysqli_stmt_bind_param($stmt,'ssss',$nome, $email, $data, $ip);
        mysqli_stmt_execute($stmt); 
        $lead->setId_lead($stmt->insert_id);
        mysqli_stmt_close($stmt);
    }
    
    public function consultaEmail(Leads $lead) 
    {
      $email = $lead->getEmail();

       $query = $this->db->getConection()->prepare("SELECT * FROM lead WHERE email=?");
       $query->bind_param('s', $email);
       $query->execute();

       $result = $query->get_result();
       $query->close();
       return count($result->fetch_all()); 
        
    }
    
    public function validaEmailBanco(Leads $lead) 
    {
      $id = $lead->getId_Lead();

       $query = $this->db->getConection()->prepare("UPDATE lead SET valida = true WHERE MD5(id_Lead)=?");
       $query->bind_param('s', $id);
       $query->execute(); 
       $query->close();
        
    }

}
 
?>